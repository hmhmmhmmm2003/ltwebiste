﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Mhltw.Models;
using Mhltw.Repository;

namespace Mhltw.Controllers
{
    [Authorize(Roles =SD.Role_Admin)]
    public class AdminController : Controller
    {

        private readonly ICategoryRepository _categoryRepository;
        private readonly IProductRepository _productRepository;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public AdminController(IProductRepository productRepository, ApplicationDbContext context,
UserManager<ApplicationUser> userManager, ICategoryRepository categoryRepository)
        {
            _productRepository = productRepository;
            _context = context;
            _userManager = userManager;
            _categoryRepository = categoryRepository;   
        }
        public async Task<IActionResult> Bill()
        {
            var bills = await _context.OrderDetails.Include(x => x.Product).ToListAsync();
            return View(bills);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var orderDetail = await _context.OrderDetails.FindAsync(id);
            if (orderDetail == null)
            {
                return NotFound();
            }
            return View(orderDetail);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id, OrderDetail orderdetail)
        {
            if (id != orderdetail.Id)
            {
                return NotFound();
            }
            var re = await _context.OrderDetails.FindAsync(id);
            _context.OrderDetails.Remove(re);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Bill));
        }

    }
}
