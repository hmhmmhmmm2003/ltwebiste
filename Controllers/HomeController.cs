using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Mhltw.Models;
using Mhltw.Repository;
using Microsoft.EntityFrameworkCore;
using Mhltw.ViewModels;

namespace Mhltw.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IProductRepository _productRepository;

        public HomeController(IProductRepository productRepository, ApplicationDbContext context)
        {
            _productRepository = productRepository;
            _context = context;
        }

        public async Task<IActionResult> Index(string name,int page = 1)
        {
            
            int pageSize = 6;
            var prods = await _context.Products.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            var chicken = await _context.Products.Where(m => m.CategoryId == 3).ToListAsync();
            var burger = await _context.Products.Where(m => m.CategoryId == 4).ToListAsync();
            var pizza = await _context.Products.Where(m => m.CategoryId == 5).ToListAsync();

            if (!string.IsNullOrEmpty(name))
            {
                prods = await _context.Products.Where(x => x.Name == name).ToListAsync();
            }
            var viewModel = new HomeViewModel
            {

                products = prods,
                Burger = burger,
                Chicken = chicken,
                Pizza   =   pizza,
                pagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = await _context.Products.CountAsync()
                }
            };

            return View(viewModel);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult ShowPage(int page = 1)
        {
            return View();
        }
    }
}
