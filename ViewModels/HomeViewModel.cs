﻿using Mhltw.Models;
using System.Reflection.Metadata;

namespace Mhltw.ViewModels
{
    public class HomeViewModel
    {
        public List<Product> Burger { get; set; }

        public List<Product> Pizza { get; set; }

        public List<Product> Chicken { get; set; }

        public List<Product> products { get; set; }
        public PagingInfo pagingInfo { get; set; }
    }
    public class PagingInfo
    {
        public int TotalItems { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages => (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
    }
}
